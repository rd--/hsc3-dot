import System.Environment {- base -}
import System.FilePath {- filepath -}

import qualified Sound.Sc3.Server.Graphdef.Io as Graphdef.Io {- hsc3 -}
import qualified Sound.Sc3.Server.Graphdef.Read as Graphdef.Read {- hsc3 -}

import qualified Sound.Sc3.Ugen.Dot.Hl as Hl {- hsc3-dot -}
import qualified Sound.Sc3.Ugen.Dot.Ll as Ll {- hsc3-dot -}

scsyndef_to_dot :: () -> FilePath -> FilePath -> IO ()
scsyndef_to_dot _opt syn_fn dot_fn = do
  gr <- Graphdef.Io.read_graphdef_file syn_fn
  let (_, gr') = Graphdef.Read.graphdef_to_graph gr
      dr = Ll.dotGraph Hl.dot_options gr'
  writeFile dot_fn dr
  return ()

scsyndef_to_dot_ext :: () -> FilePath -> IO ()
scsyndef_to_dot_ext opt syn_fn = scsyndef_to_dot opt syn_fn (replaceExtension syn_fn "dot")

scsyndef_draw :: () -> FilePath -> IO ()
scsyndef_draw _opt syn_fn = do
  gr <- Graphdef.Io.read_graphdef_file syn_fn
  let (_, gr') = Graphdef.Read.graphdef_to_graph gr
  Hl.draw gr'

usage :: String
usage =
  unlines
    [ "hsc3-dot cmd arg"
    , " scsyndef-draw syn-file"
    , " scsyndef-to-dot syn-file dot-file"
    , " scsyndef-to-dot-ext syn-file..."
    ]

main :: IO ()
main = do
  arg <- getArgs
  case arg of
    ["scsyndef-draw", syn_fn] -> scsyndef_draw () syn_fn
    ["scsyndef-to-dot", syn_fn, dot_fn] -> scsyndef_to_dot () syn_fn dot_fn
    "scsyndef-to-dot-ext" : nm -> mapM_ (scsyndef_to_dot_ext ()) nm
    _ -> putStrLn usage
