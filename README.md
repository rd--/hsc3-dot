hsc3-dot - haskell supercollider dot
------------------------------------

[hsc3-dot](http://rohandrape.net/t/hsc3-dot)
is a
[haskell](http://haskell.org/)
library to write
[dot](http://www.graphviz.org/doc/info/lang.html)
graph files of
[SuperCollider](http://audiosynth.com/)
unit generator graphs.
It is part of the
[hsc3](http://rohandrape.net/t/hsc3)
project.

Graph rendering is by
[graphviz](http://graphviz.org/).

There is a
[help file](http://rohandrape.net/?t=hsc3-dot&e=help/dot.help.lhs)
and also an
[entry](http://rohandrape.net/?t=hsc3-texts&e=2.2-drawing.md)
at
[hsc3-texts](http://rohandrape.net/?t=hsc3-texts).

The default
[SVG](http://www.w3.org/Graphics/SVG/)
viewer is
[chromium](https://www.chromium.org/)
which is in the debian package of the same name.

<!-- [rsvg-view-3]( http://wiki.gnome.org/LibRsvg),
which is in the debian package `librsvg2-bin` -->

## cli

[hsc3-dot](http://rohandrape.net/?t=hsc3-dot&e=md/dot.md)

© [rohan drape](http://rohandrape.net/), 2006-2025,
  [gpl](http://gnu.org/copyleft/)

* * *

```
$ make doctest
Examples: 6  Tried: 6  Errors: 0  Failures: 0
$
```
