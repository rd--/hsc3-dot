module Sound.Sc3.Ugen.Dot.Common where

import Data.Char {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

-- * List

-- | Bracket with elements.
bracket1 :: (a, a) -> [a] -> [a]
bracket1 (l, r) x = [l] ++ x ++ [r]

-- | Bracket with lists.
bracket :: ([a], [a]) -> [a] -> [a]
bracket (l, r) x = l ++ x ++ r

-- | 'bracket' with double quotes.
string_pp :: String -> String
string_pp = bracket1 ('"', '"')

-- | 'bracket' with double quotes and braces.
label_pp :: String -> String
label_pp = bracket ("\"{", "}\"")

-- | Type specialised 'show'.
int_pp :: Int -> String
int_pp = show

{- | 'reverse' of /f/ of 'reverse'.

>>> let drop_while_right f = right_variant (dropWhile f)
>>> drop_while_right isUpper "abcDEF"
"abc"
-}
right_variant :: ([a] -> [b]) -> [a] -> [b]
right_variant f = reverse . f . reverse

-- * Numbers

{- | Limited precision PP for 'n', no scientific notation.

>>> map (limit_precision_p True 2) [1,1.25,1.12345,0,0.05,pi*1e8,1e9]
["1.00","1.25","1.12","0.00","0.05","314159265.36","1000000000.00"]
-}
limit_precision_p :: (PrintfArg n) => Bool -> Int -> n -> String
limit_precision_p r n c =
  let i = printf "%.*f" n c
  in if r
      then i
      else right_variant (dropWhile (== '.') . dropWhile (== '0')) i

{- | Limited precision PP for 'n', with scientific notation.

>>> map (limit_precision_e 3) [1,1.25,0.05,pi*1e8,1e9,read "Infinity"]
["1","1.25","5e-2","3.142e8","1e9","Infinity"]
-}
limit_precision_e :: (Read n, Show n) => Int -> n -> String
limit_precision_e n c =
  case show c of
    "Infinity" -> "Infinity"
    c' ->
      let (i, j') = break (== '.') c'
          j = case uncons j' of
            Nothing -> error ("limit_precision_e: no dot: " ++ c')
            Just (_, t) -> t
          (k, l) = break (== 'e') j
          f :: String -> Int
          f m = round ((read (take (n + 1) m) :: Read n => n) / (10 :: Double))
          k' =
            if length k > n
              then show (f k)
              else k
      in if k == "0"
          then i ++ l
          else i ++ "." ++ k' ++ l

{- | Variant selecting scientific notation more cautiously than haskells default PP.

>>> map (limit_precision True 3) [1,1.25,0.05,0,pi*1e8,1e9]
["1.000","1.250","0.050","0.000","3.142e8","1e9"]
-}
limit_precision :: (Floating n, Ord n, Read n, Show n, PrintfArg n) => Bool -> Int -> n -> String
limit_precision r n c =
  if c /= 0 && (c < (10 ** fromIntegral (-n)) || c > 1e6)
    then limit_precision_e n c
    else limit_precision_p r n c

-- * Attr

-- | Key value pair.
type Attr = (String, String)

-- | Dot attributes are written @key=value@.
attr_pp :: Attr -> String
attr_pp (k, v) = k ++ "=" ++ v

-- | If @value@ is 'null' then 'Nothing'.
attr_pp_maybe :: Attr -> Maybe String
attr_pp_maybe (k, v) = if null v then Nothing else Just (attr_pp (k, v))

-- | Attribute lists are in square brackets and comma seperated.
attr_list_pp :: [Attr] -> String
attr_list_pp = bracket1 ('[', ']') . intercalate "," . mapMaybe attr_pp_maybe

-- | Attribute sets are named and semi-colon terminated.
attr_set_pp :: String -> [Attr] -> String
attr_set_pp nm attr = concat [nm, " ", attr_list_pp attr, ";"]

-- | Size is given as @(width,height)@.
size_pp :: (Double, Double) -> String
size_pp (x, y) = printf "\"%f,%f\"" x y

-- | 'toLower' of 'show'.
bool_pp :: Bool -> String
bool_pp = map toLower . show

-- * String

-- | Very rudimentary HTML escaping.
escape_html :: String -> String
escape_html =
  let t = [('<', "&lt;"), ('>', "&gt;"), ('&', "&amp;")]
      f c = fromMaybe [c] (lookup c t)
  in concatMap f
