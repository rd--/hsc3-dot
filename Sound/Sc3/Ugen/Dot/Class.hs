-- | Typeclass for 'Drawable' values.
module Sound.Sc3.Ugen.Dot.Class where

import qualified Sound.Sc3 as Sc3 {- hsc3 -}
import qualified Sound.Sc3.Ugen.Graph as Graph {- hsc3 -}

import qualified Sound.Sc3.Ugen.Dot.Ll as Ll {- hsc3-dot -}
import qualified Sound.Sc3.Ugen.Dot.Type as Type {- hsc3-dot -}

-- | 'Drawable' values can render dot graphs given 'Type.Dot_Options'.
class Drawable a where
  dot_with_opt :: Type.Dot_Options -> a -> String

-- | 'Graph.U_Graph's are 'Drawable', by 'Ll.dotGraph'.
instance Drawable Graph.U_Graph where
  dot_with_opt = Ll.dotGraph

-- | 'Sc3.Ugens's are 'Drawable', by 'Graph.ugen_to_graph'.
instance Drawable Sc3.Ugen where
  dot_with_opt o = Ll.dotGraph o . Graph.ugen_to_graph

-- | 'S.Synthdef's are 'Drawable', by 'Sc3.synthdefGraph'.
instance Drawable Sc3.Synthdef where
  dot_with_opt o = Ll.dotGraph o . Sc3.synthdefGraph
