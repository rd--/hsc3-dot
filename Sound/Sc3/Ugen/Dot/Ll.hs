-- | Low-level implementation of @Dot@ language writer.
module Sound.Sc3.Ugen.Dot.Ll where

import Control.Monad {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import System.FilePath {- filepath -}
import System.Process {- process -}
import Text.Printf {- base -}

import qualified Sound.Sc3 as Sc3 {- hsc3 -}
import qualified Sound.Sc3.Common.Base.System as Base.System {- hsc3 -}
import qualified Sound.Sc3.Ugen.Graph as Graph {- hsc3 -}

import Sound.Sc3.Ugen.Dot.Common {- hsc3-dot -}
import Sound.Sc3.Ugen.Dot.Type {- hsc3-dot -}

-- | Basic attribute sets given 'Dot_Options'.
std_style :: Dot_Options -> [String]
std_style o =
  [ attr_set_pp
      "graph"
      [ ("splines", bool_pp (use_splines o))
      , ("size", maybe "" size_pp (graph_size o))
      , ("rankdir", graph_dir o)
      ]
  , attr_set_pp
      "node"
      [ ("fontsize", int_pp (font_size o))
      , ("fontname", string_pp (font_name o))
      ]
  , attr_set_pp
      "edge"
      [ ("arrowhead", "box")
      , ("arrowsize", "0.25")
      ]
  ]

-- | Generate dot representation of the provided unit generator graph.
dotGraph :: Dot_Options -> Graph.U_Graph -> String
dotGraph o g =
  let (Graph.U_Graph _ _ ks us) = g
      ls =
        concat
          [ ["digraph Anonymous {"]
          , std_style o
          , if inline_controls o
              then []
              else map (dot_node_k o) ks
          , map (dot_node_u o g) us
          , map (dot_edge o g) (Graph.ug_edges g)
          , ["}"]
          ]
  in unlines (filter (not . null) ls)

-- | View @dot@ graph according to 'Dot_Options'.
view_with :: Dot_Options -> String -> IO ()
view_with o x = do
  v <- get_viewer o
  let d = output_directory o
      f = d </> output_file_name o
      f_dot = f <.> "dot"
      f_svg = f <.> "svg"
      f_svg_gz = f_svg <.> "gz"
      f_pdf = f <.> "pdf"
      f_view = case output_format o of
        Dot -> f_dot
        Svg -> f_svg
        Svg_Gz -> f_svg_gz
        Pdf -> f_pdf
      gen_svg = rawSystem "dot" ["-T", "svg", f_dot, "-o", f_svg]
  writeFile f_dot x
  when (output_format o `elem` [Svg, Svg_Gz]) (void gen_svg)
  when (output_format o == Svg_Gz) (void (rawSystem "gzip" ["-f", f_svg]))
  -- import System.Directory {- directory -}
  -- when (output_format o /= Dot) (removeFile f_dot)
  void (rawSystem v [f_view])

-- | Input port.
data IP
  = IP_Label String
  | IP_Port String Char Int
  | IP_Const (Maybe String, String, Int) Sc3.Sample

-- | Variant of 'limit_precision' reading parameters from 'Dot_Options'.
limit_precision_o :: Dot_Options -> Sc3.Sample -> String
limit_precision_o o = limit_precision (indicate_precision o) (numeric_precision o)

-- | IP in record form.
ip_record :: Dot_Options -> IP -> String
ip_record o i =
  case i of
    IP_Label l -> l
    IP_Port _ d n -> '<' : d : '_' : show n ++ ">"
    IP_Const _ n -> limit_precision_o o n

-- | 'IP' as @HTML@ string.
ip_html :: Dot_Options -> IP -> String
ip_html o i =
  case i of
    IP_Label l -> printf "<TD>%s</TD>" (escape_html l)
    IP_Port u c n ->
      let p = c : '_' : show n
          a = if use_attr_id o then printf " ID=\"%s:%s\"" u p else ""
      in printf "<TD PORT=\"%s\"%s></TD>" p a
    IP_Const (k, u, p) n ->
      let p' = "K_" ++ show p
          n' = limit_precision_o o n
          l = maybe "" (++ ":") k ++ n'
          a = if use_attr_id o then printf " ID=\"%s:%s\"" u p' else ""
      in printf "<TD%s>%s</TD>" a l

record :: Dot_Options -> String -> String -> ([IP], [IP]) -> String
record o nm clr (upr, lwr) =
  let f l = concat ["{", g (map (ip_record o) l), "}"]
      g = intercalate "|"
      lbl = g [f upr, f lwr]
  in attr_set_pp
      nm
      [ ("shape", string_pp "record")
      , ("color", string_pp clr)
      , ("label", label_pp lbl)
      ]

table :: Dot_Options -> String -> String -> ([IP], [IP]) -> String
table o lbl clr (upr, lwr) =
  let k = length upr - length lwr
      e = concat (replicate k "<TD BORDER=\"0\"></TD>")
      f p l =
        if null l
          then ""
          else concat ["<TR>", p ++ concatMap (ip_html o) l, "</TR>"]
  in attr_set_pp
      lbl
      [ ("shape", string_pp "plaintext")
      , ("color", string_pp clr)
      ,
        ( "label"
        , concat
            [ "<<TABLE BORDER=\"0\" CELLBORDER=\"1\">"
            , f [] upr ++ f e lwr
            , "</TABLE>>"
            ]
        )
      ]

dot_edge :: Dot_Options -> Graph.U_Graph -> Graph.U_Edge -> String
dot_edge o g (l, Graph.To_Port ri rn) =
  case Graph.ug_find_node g (Graph.from_port_nid l) of
    Just ln ->
      let s = if fix_edge_location o then ":s" else ""
          clr = if colour_edges o then Sc3.rate_color (Graph.u_node_rate ln) else "black"
      in if Graph.is_u_node_c ln || Graph.is_u_node_k ln && inline_controls o
          then ""
          else
            concat
              [ Graph.u_node_label ln
              , if Graph.is_u_node_u ln
                  then ":o_" ++ show (Graph.port_idx_or_zero l) ++ s
                  else
                    if Graph.is_u_node_k ln
                      then ":o_0"
                      else ""
              , " -> "
              , let ri_n = fromJust (Graph.ug_find_node g ri)
                in Graph.u_node_label ri_n
              , ":i_"
              , show rn
              , " [color="
              , clr
              , "]"
              , ";"
              ]
    _ -> error "else"

input :: Dot_Options -> Graph.U_Graph -> Graph.U_Node -> Graph.From_Port -> Int -> IP
input o g u fp k =
  case fp of
    Graph.From_Port_C i ->
      case Graph.ug_find_node g i of
        Nothing -> error (show ("input?", i, g, u, fp, k))
        Just n -> IP_Const (Nothing, Graph.u_node_label u, k) (Graph.u_node_c_value n)
    Graph.From_Port_K i _ ->
      if inline_controls o
        then
          let n = fromJust (Graph.ug_find_node g i)
              l =
                if display_control_names o
                  then Just (Graph.u_node_k_name n)
                  else Nothing
          in IP_Const (l, Graph.u_node_label u, k) (Graph.u_node_k_default n)
        else IP_Port (Graph.u_node_label u) 'i' k
    _ -> IP_Port (Graph.u_node_label u) 'i' k

dot_node_u :: Dot_Options -> Graph.U_Graph -> Graph.U_Node -> String
dot_node_u o g u =
  let lbl = Graph.u_node_label u
      clr = Sc3.rate_color (Graph.u_node_rate u)
      i = Graph.u_node_u_inputs u
      i' = length i - 1
      s = Graph.u_node_u_special u
      upr =
        IP_Label (Sc3.ugen_user_name (Graph.u_node_u_name u) s)
          : zipWith (input o g u) i [0 .. i']
      lwr =
        let n = length (Graph.u_node_u_outputs u) - 1
        in map (IP_Port (Graph.u_node_label u) 'o') [0 .. n]
      f = if use_tables o then table o else record o
  in if Graph.u_node_is_control u || Graph.u_node_is_implicit u
      then ""
      else f lbl clr (upr, lwr)

dot_node_k_color :: Graph.U_Node -> Attr
dot_node_k_color k =
  let c =
        if Graph.u_node_k_type k == Sc3.K_TriggerRate
          then "cyan"
          else Sc3.rate_color (Graph.u_node_k_rate k)
  in ("color", string_pp c)

dot_node_k_rec :: Dot_Options -> Graph.U_Node -> String
dot_node_k_rec o u =
  attr_set_pp
    (Graph.u_node_label u)
    [ ("shape", string_pp "rect")
    , dot_node_k_color u
    , ("label", string_pp (Graph.u_node_k_name u ++ ":" ++ limit_precision_o o (Graph.u_node_k_default u)))
    ]

dot_node_k_html :: Dot_Options -> Graph.U_Node -> String
dot_node_k_html o u =
  attr_set_pp
    (Graph.u_node_label u)
    [ ("shape", string_pp "plaintext")
    , dot_node_k_color u
    ,
      ( "label"
      , concat
          [ "<<TABLE BORDER=\"0\" CELLBORDER=\"1\">"
          , "<TR><TD PORT=\"o_0\">"
          , Graph.u_node_k_name u
          , ":"
          , limit_precision_o o (Graph.u_node_k_default u)
          , "</TD></TR>"
          , "</TABLE>>"
          ]
      )
    ]

dot_node_k :: Dot_Options -> Graph.U_Node -> String
dot_node_k o =
  if use_tables o
    then dot_node_k_html o
    else dot_node_k_rec o

{- | Considering 'output_format' read either the environment variable DOTVIEWER or SVGVIEWER.
The default values are 'dot_viewer' and 'svg_viewer'.
-}
get_viewer :: Dot_Options -> IO String
get_viewer o =
  if output_format o == Dot
    then Base.System.get_env_default "DOTVIEWER" (dot_viewer o)
    else Base.System.get_env_default "SVGVIEWER" (svg_viewer o)
