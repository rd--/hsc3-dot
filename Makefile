all:
	echo "hsc3-dot"

clean:
	rm -Rf dist dist-newstyle *~
	(cd cmd ; make clean)

mk-cmd:
	(cd cmd ; make all install clean)

push-all:
	r.gitlab-push.sh hsc3-dot

indent:
	fourmolu -i Sound cmd

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound
