# hsc3-dot

## scsyndef-to-dot (June, 2014)

A _disassembler_ for UGen graphs, it reads the binary representation
of a [SuperCollider](http://audiosynth.com) _instrument_ and runs
[hsc3-dot](?t=hsc3-dot) on the unit-generator graph writing.

`scsyndef-to-dot-ext` operates on a set of scsyndef files, deriving
the dot file names by replacing the file extension.

`scsyndef-draw` write the dot file to a temporary file and runs `DOTVIEWER`.

<!--
The _dot_ graph of the binary encoding of the
[why supercollider?](?t=hsc3-graphs&e=lib/sc/graph/jmcc-why-supercollider.scd)
graph is:

![](sw/hsc3-graphs/svg/jmcc-why-supercollider.sc.svg)
-->

### Rationale

Drawings of `UGen` graphs are not dependent on the
language used to generate the graph, ie. they do not record
the processes that contructed the graph, only the graph structure.

By disassembling the binary encoding of a graph, the [hsc3](?t=hsc3)
graph drawing codes can be used in other supercollider synthesiser
contexts.

Immediately, it makes maintenance of [rsc3-dot](?t=rsc3-dot) and
[sc3-dot](?t=sc3-dot) strictly unnecessary.
