{- | Plain graph.

A very simple graph drawing that labels vertices with the UGen name
and rate and draws edges without port indicators.
-}
module Sound.Sc3.Ugen.Dot.Plain where

import Data.Char {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

import Sound.Sc3 {- hsc3 -}
import Sound.Sc3.Ugen.Graph {- hsc3 -}

-- | Ugen name.
type Name = String

-- | Vertex as (id,(rate,name))
type V = (Id, (Rate, Name))

-- | Make V from U_Node, error if node is not U_Node_U
mk_v :: U_Node -> Maybe V
mk_v n =
  case n of
    U_Node_U {} ->
      let k = u_node_id n
      in if k >= 0 then Just (k, (u_node_rate n, u_node_user_name n)) else Nothing
    _ -> error "mk_v"

-- | Edge as (id,id)
type E = (Id, Id)

mk_e :: [Id] -> U_Edge -> Maybe E
mk_e v (p, q) =
  let (i, j) = (from_port_nid p, to_port_nid q)
  in if i `elem` v && j `elem` v then Just (i, j) else Nothing

type G = ([V], [E])

mk_g :: Ugen -> G
mk_g u =
  let g = ugen_to_graph u
      v = mapMaybe mk_v (ug_ugens g)
      k = map fst v
      e = nub (mapMaybe (mk_e k) (ug_edges g))
  in (v, e)

v_pp :: V -> String
v_pp (k, (rt, nm)) =
  printf
    "%d [label=\"%s.%s\",color=%s];"
    k
    nm
    (map toLower (rateAbbrev rt))
    (rate_color rt)

e_pp :: E -> String
e_pp (i, j) = printf "%d -> %d;" i j

g_pp :: [String] -> G -> String
g_pp attr (v, e) =
  let pre =
        [ "digraph g {"
        , "node [shape=rectangle];"
        ]
      post = ["}"]
  in unlines (concat [pre, attr, map v_pp v, map e_pp e, post])

ugen_to_dot :: [String] -> Ugen -> String
ugen_to_dot attr = g_pp attr . mk_g
