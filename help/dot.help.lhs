hsc3-dot
========

Library to provide graph drawing of unit generator graphs.  The
output is in the form of a dot graph, which can be drawn using the
[graphviz](http://graphviz.org) tools.

> import Sound.SC3 {- hsc3 -}
> import Sound.SC3.UGen.Dot {- hsc3-dot -}
> import qualified Sound.SC3.UGen.Dot.Plain {- hsc3-dot -}

> drw nm f =
>   let o = dot_options {output_directory = "/home/rohan/sw/hsc3-dot/dot"
>                       ,output_file_name = nm}
>   in draw_with_opt (f o)

Simple a-rate only graph.
(When using the hsc3 [emacs](https://www.gnu.org/s/emacs/) mode
typing `C-cC-g` will draw the graph of the value at the cursor.)

> g_00 = out 0 (sinOsc ar 440 0 * 0.1)

> d_00 = drw "01" id g_00

![](sw/hsc3-dot/svg/01.svg)

As above, but written using the `record` interface.

> d_01 =
>   let f o = o {use_tables = False}
>   in drw "02" f g_00

![](sw/hsc3-dot/svg/02.svg)

With k-rate subgraph.

> g_02 =
>   let f = lfSaw kr 1 (-1) * 220 + 440
>   in out 0 (sinOsc ar f 0 * 0.1)

> d_02 = drw "03" id g_02

![](sw/hsc3-dot/svg/03.svg)

With k-rate & i-rate subgraphs

> g_03 =
>   let l = randId 'α' 200 400
>       m = randId 'β' l 600
>       a = randId 'γ' 500 900
>       f = lfSaw kr 1 (-1) * m + a
>   in out 0 (sinOsc ar f 0 * 0.1)

> d_03 = drw "04" id g_03

![](sw/hsc3-dot/svg/04.svg)

With control input

> g_04 =
>   let f = control kr "freq" 440
>   in out 0 (sinOsc ar f 0 * 0.1)

> d_04 = drw "05" id g_04

![](sw/hsc3-dot/svg/05.svg)

As above but with inlined controls.

> d_05 =
>   let f o = o {inline_controls = True}
>   in drw "06" f g_04

![](sw/hsc3-dot/svg/06.svg)

As above but without control name label.

> d_06 =
>   let f o = o {inline_controls = True
>               ,display_control_names = False}
>   in drw "07" f g_04

![](sw/hsc3-dot/svg/07.svg)

With multiple channel expansion.

> g_07 =
>   let f = mce2 440 220
>   in out 0 (sinOsc ar f 0 * 0.1)

> d_07 = drw "08" id g_07

![](sw/hsc3-dot/svg/08.svg)

With multiple root graph.

> g_08 =
>   let f = mce2 440 220 + in' 2 kr 0
>       o1 = sinOsc ar f 0 * 0.1
>       o2 = sinOsc kr (mce2 0.25 0.35) 0 * mce2 10 15
>   in mrg2 (out 0 o1) (out 1 o2)

> d_08 = drw "09" id g_08

![](sw/hsc3-dot/svg/09.svg)

With multiple channel UGen.

> g_09 = out 0 (pan2 (sinOsc ar 440 0 * 0.1) 0 1)

> d_09 = drw "10" id g_09

![](sw/hsc3-dot/svg/10.svg)

With reserved labels (ie. <,>,&), and fixed size graph (size in inches).

> g_10 = out 0 (sinOsc ar 440 0 `greater_than` 0)

> d_10 =
>   let f o = (to_svg_options o) {graph_size = Just (1,4)}
>   in drw "11" f g_10

![](sw/hsc3-dot/svg/11.svg)

Draw left to right.
The record format automatically adjusts, the HTML/TABLE format does not.

> d_11 = drw "12" (\o -> o {graph_dir = "LR",use_tables = False}) g_08

![](sw/hsc3-dot/svg/12.svg)

There is also a function `draw_plain_with_opt` which draws a simpler graph
where vertices are simple boxes without input and output port indicators.

> d_12 =
>   let nm = "13"
>       o = dot_options {output_directory = "/home/rohan/sw/hsc3-dot/dot"
>                       ,output_file_name = nm}
>   in draw_plain_with_opt o g_08


![](sw/hsc3-dot/svg/13.svg)

> main = sequence [d_00,d_01,d_02,d_03,d_04,d_05,d_06,d_07,d_08,d_09,d_10,d_11,d_12]
