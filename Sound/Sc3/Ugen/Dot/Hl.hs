-- | High-level dot.
module Sound.Sc3.Ugen.Dot.Hl where

import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Ugen.Dot.Class as Class
import qualified Sound.Sc3.Ugen.Dot.Ll as Ll
import qualified Sound.Sc3.Ugen.Dot.Plain as Plain
import qualified Sound.Sc3.Ugen.Dot.Type as Type

-- | 'Class.dot_with_opt' of 'dot_options'.
dot :: Class.Drawable a => a -> String
dot = Class.dot_with_opt dot_options

-- | 'Class.dot_with_opt' of 'svg_options'.
dot_svg :: Class.Drawable a => a -> String
dot_svg = Class.dot_with_opt svg_options

-- | 'view_with' of 'Class.dot_with_opt'.
draw_with_opt :: Class.Drawable a => Type.Dot_Options -> a -> IO ()
draw_with_opt o x = Ll.view_with o (Class.dot_with_opt o x)

-- | 'draw_with_opt' with 'run_viewer' set to 'False'.
write_with_opt :: Class.Drawable a => Type.Dot_Options -> a -> IO ()
write_with_opt o = draw_with_opt (o {Type.run_viewer = False})

{- | 'draw_with_opt' of 'dot_options'.

> draw (out 0 (sinOsc ar (mce2 440 441) 0 * 0.1))
-}
draw :: Class.Drawable a => a -> IO ()
draw = draw_with_opt dot_options

{- | 'draw_with_opt' of 'svg_options'.

> draw_svg (out 0 (sinOsc ar (mce2 440 441) 0 * 0.1))
-}
draw_svg :: Class.Drawable a => a -> IO ()
draw_svg = draw_with_opt svg_options

-- | 'Ll.view_with' of 'Plain.ugen_to_dot'.
draw_plain_with_opt :: Type.Dot_Options -> Ugen -> IO ()
draw_plain_with_opt o = Ll.view_with o . Plain.ugen_to_dot (Ll.std_style o)

{- | 'draw_plain_with_opt' of 'dot_options'

> draw_plain (out 0 (sinOsc ar (mce2 440 441) 0 * 0.1))
-}
draw_plain :: Ugen -> IO ()
draw_plain = draw_plain_with_opt dot_options

{- | Default @dot@ format 'Type.Dot_Options'.

>>> Ll.std_style dot_options
["graph [splines=false,rankdir=TB];","node [fontsize=12,fontname=\"Courier\"];","edge [arrowhead=box,arrowsize=0.25];"]
-}
dot_options :: Type.Dot_Options
dot_options =
  Type.Dot_Options
    { Type.use_tables = True
    , Type.use_attr_id = False
    , Type.use_splines = False
    , Type.output_format = Type.Dot
    , Type.output_directory = "/tmp"
    , Type.output_file_name = "hsc3"
    , Type.fix_edge_location = False
    , Type.numeric_precision = 3
    , Type.indicate_precision = False
    , Type.inline_controls = False
    , Type.display_control_names = True
    , Type.dot_viewer = "dotty" -- dot -Txlib
    , Type.svg_viewer = "chromium" -- "rsvg-view-3" "inkview"
    , Type.pdf_viewer = "chromium" -- "rsvg-view-3" "inkview"
    , Type.font_name = "Courier"
    , Type.font_size = 12
    , Type.graph_size = Nothing
    , Type.colour_edges = True
    , Type.run_viewer = True
    , Type.graph_dir = "TB"
    }

-- | Set 'output_format' to 'Svg' and 'fix_edge_location' to 'True'.
to_svg_options :: Type.Dot_Options -> Type.Dot_Options
to_svg_options o = o {Type.output_format = Type.Svg, Type.fix_edge_location = True}

-- | 'to_svg_options' of 'dot_options'.
svg_options :: Type.Dot_Options
svg_options = to_svg_options dot_options
