-- | Re-export of three main modules.
module Sound.Sc3.Ugen.Dot (module M) where

import Sound.Sc3.Ugen.Dot.Class as M
import Sound.Sc3.Ugen.Dot.Hl as M
import Sound.Sc3.Ugen.Dot.Type as M
